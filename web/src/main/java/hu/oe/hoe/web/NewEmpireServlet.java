/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.hoe.web;

import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.EmpireRepository;
import hu.oe.hoe.adatok.People;
import hu.oe.hoe.adatok.Population;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "NewEmpireServlet" ,urlPatterns = {"/NewEmpireServlet"})
public class NewEmpireServlet extends HttpServlet {

    @Inject
    BuildingRepository buildingRepository;
    
    @Inject
    EmpireRepository empireRepo;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User sess = ((User)request.getSession().getAttribute("user"));
        
        Empire empire = new Empire(request.getParameter("ename"), request.getParameter("desc"), sess);
        
      
        empire.getPopulation().add(new Population(new People("Sanyika", "SSASA"), (long) 1));
        empireRepo.add(empire);
        //response.getWriter().println(".....Birodalom hozzaadva.....");
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(0).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(0).getQuantity());
        
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(1).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(1).getQuantity());
        
        //response.getWriter().println(sess.getEmpire().get(0).getProduce().get(2).getAsset().getName() + sess.getEmpire().get(0).getProduce().get(2).getQuantity());
        request.setAttribute("buildings", buildingRepository);
        
        
        //if (request.getParameter("ename").equals("")) {
            request.setAttribute("currentEmpire", empire);
        //}
        //else{
        //request.setAttribute("currentEmpire", empire);
        //}
        
        
        getServletContext().getRequestDispatcher("/building.jsp").include(request, response);
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
