package hu.oe.hoe.web;

import hu.oe.hoe.adatok.LoginException;
import hu.oe.hoe.adatok.RegistrationException;
import hu.oe.hoe.adatok.User;
import hu.oe.hoe.adatok.UserRepository;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserService {
    
    @Inject
    UserRepository userRepository;
    
    public void Registration(String rName, String rPassword) throws RegistrationException {        
        if(userRepository.isUserExistByName(rName) == false) {
            userRepository.Add(new User(rName, rPassword, false));
        }
        else {
            throw new RegistrationException();
        } 
    }
    
    public User Login(String rName, String rPassword) throws LoginException {
        try {
            User user = userRepository.getByNamePassword(rName, rPassword); 
            return user;
        }
        catch(Exception e) {throw new LoginException();}
    }
    
}
