package hu.oe.hoe.adatok;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Hybrid")
public class Hybrid {
    
    //id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id; 
    
    //fields
    private byte percent;
    
    //relations
    @ManyToOne
    private Species species;
    
    //constrcutor
    public Hybrid(Species species, byte percent) {
        this.species = species;
        this.percent = percent;
    }

    public Hybrid() {
    }
    
    //get/set-s
    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public byte getPercent() {
        return percent;
    }

    public void setPercent(byte percent) {
        this.percent = percent;
    }
    
}
