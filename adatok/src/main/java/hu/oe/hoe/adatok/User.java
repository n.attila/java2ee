package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class User {
    
    //id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    
    //fields
    private String name;
    private String password;
    private boolean admin;
    
    //relations
    @OneToMany(mappedBy = "User")    
    private List<Hero> heroes = new ArrayList<>();
    
    @OneToMany(mappedBy = "user")
    private List<Empire> empire = new ArrayList<>();

    //constructors
    public User(String name, String password, boolean admin) {
        this.name = name;
        this.password = password;
        this.admin = admin;
    }

    public User() {
    }
    
    //get/set-s
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
      public List<Empire> getEmpire() {
        return empire;
    }

    public void setEmpire(List<Empire> empire) {
        this.empire = empire;
    }
    
    
}
