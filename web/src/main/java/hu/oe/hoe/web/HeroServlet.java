package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Hero;
import hu.oe.hoe.adatok.Hybrid;
import hu.oe.hoe.adatok.Species;
import hu.oe.hoe.adatok.SpeciesRepository;
import hu.oe.hoe.adatok.User;
import hu.oe.hoe.adatok.UserRepository;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "HeroServlet", urlPatterns = {"/newHero"})
public class HeroServlet extends HttpServlet {

    //injects    
    @Inject
    SpeciesRepository speciesRepository;
    
    @EJB
    HeroService heroService;
    
    //HTTP methods
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        
        Hero newHero = new Hero(request.getParameter("name"), request.getParameter("desc"));
        List<Species> tmp =speciesRepository.getSpecies(); 
        for(Species sp: tmp) {
            Hybrid newHybrid = new Hybrid(sp, Byte.parseByte(request.getParameter(sp.getName())));
            newHero.getHybrids().add(newHybrid);            
        }

        User sessUser = ((User)request.getSession().getAttribute("user"));
        newHero.setUser(sessUser);
        
        heroService.add(newHero);
        
        //sessUser.getHeroes().add(newHero);
       
        for(Hero hero: heroService.getHeroes())
            response.getWriter().print("<br />" + hero.getName());
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
