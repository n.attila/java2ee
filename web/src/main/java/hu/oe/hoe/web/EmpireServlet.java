package oe;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.People;
import hu.oe.hoe.adatok.Population;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author javaee
 */
@WebServlet(urlPatterns = {"/EmpireServlet"})
public class EmpireServlet extends HttpServlet {

   @Inject
   BuildingRepository buildingRepository;
 

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User sess = ((User)request.getSession().getAttribute("user"));
        
        Empire selectedEmpire = new Empire();
        
        for (Empire selectedEm: sess.getEmpire()) {
            if (selectedEm.getName().equals(request.getParameter("selectEmpire"))) {
                selectedEmpire = selectedEm;
            }
        }
      
        
        selectedEmpire.getPopulation().add(new Population(new People("First Person", "The first percon"), (long)1));
        
       
       
        request.setAttribute("buildings", buildingRepository.getBuildings());
        
        request.setAttribute("currentEmpire", selectedEmpire);
        
        getServletContext().getRequestDispatcher("/building.jsp").include(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
