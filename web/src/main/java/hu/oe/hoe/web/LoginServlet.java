package hu.oe.hoe.web;

import hu.oe.hoe.adatok.LoginException;
import hu.oe.hoe.adatok.SpeciesRepository;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    @Inject
    SpeciesRepository speciesRepository;
    
    @Inject
    UserService userService;
    
    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        String name = request.getParameter("name");
        String password = request.getParameter("password");       

        try{
            request.getSession().setAttribute("user", userService.Login(name, password));
            request.setAttribute("species", speciesRepository.getSpecies());
             getServletContext().getRequestDispatcher("/user.jsp").include(request, response);
        }
        catch (LoginException e) {     
            response.getWriter().print("Sikertelen bejelentkezés.");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
