package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class SpeciesRepository {
   
    //entity manager
    private EntityManager em = Persistence.createEntityManagerFactory("Hero").createEntityManager();
    
    //get/set-s
    public List<Species> getSpecies() {
        return em.createQuery("SELECT s FROM Species s", Species.class).getResultList(); //JPQL lekérdezés (Java Persistance Query Language)
    }
        
    //methods
    public void Add(Species specie) {
        em.getTransaction().begin();
        em.persist(specie);
        em.getTransaction().commit();        
    }
}
