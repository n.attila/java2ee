package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Hero;
import hu.oe.hoe.adatok.HeroRepository;
import hu.oe.hoe.adatok.Hybrid;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless 
public class HeroService {
    
   
    @Inject
    HeroRepository heroRepository;
    
    
    public Hero add(Hero hero) {
        byte sum = 0;
        
        if(hero != null && hero.getHybrids() != null) {
            for(Hybrid h: hero.getHybrids()) {
                sum += h.getPercent();
            }
        }
   heroRepository.Add(hero);
        return hero;
    }
    
    public List<Hero> getHeroes() {
        return heroRepository.getHeroes();
    }
}
