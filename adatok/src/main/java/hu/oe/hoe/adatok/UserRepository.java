package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UserRepository {
    
    
    private EntityManager em = Persistence.createEntityManagerFactory("Hero").createEntityManager();
        
   
    public void Add(User user) {
        em.persist(user);  
        em.flush();
    }
    
    public User getByNamePassword(String name, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("name"), name), cb.equal(root.get("password"), password)));
        return (User)em.createQuery(cq).getSingleResult();
    }
    
    public boolean isUserExistByName(String name) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery(User.class);
            Root root = cq.from(User.class);
            cq.select(root);
            cq.where(cb.equal(root.get("name"), name));
            User user = (User)em.createQuery(cq).getSingleResult();
            return true;
        } 
        catch(Exception e) {
            return false;
        }
        
    }    
    
   
    public List<User> getUsers() {
        return em.createQuery("SELECT s FROM User s", User.class).getResultList();
    }
   
}
