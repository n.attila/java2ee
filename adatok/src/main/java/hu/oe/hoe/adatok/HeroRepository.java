package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class HeroRepository {
   
    //entity manager
    private EntityManager em = Persistence.createEntityManagerFactory("Hero").createEntityManager();
        
    //fields
    private List<Hero> heroes = new ArrayList<>();
        
    //get/set-s
    public List<Hero> getHeroes() {
        return em.createQuery("SELECT s FROM Hero s", Hero.class).getResultList(); //JPQL lekérdezés (Java Persistance Query Language)
    }
        
    //methods
    public void Add(Hero hero) {
        for(Hybrid h:hero.getHybrids()) {
            em.persist(h);
        }
        em.persist(hero);
    }
    
    public List<Hero> getByNameUser(String name, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(Hero.class);
        Root root = cq.from(Hero.class);
        cq.select(root);
        cq.where(cb.and(
                cb.equal(root.get("name"), name),
                cb.equal(root.get("user"), user)
                ));
        return em.createQuery(cq).getResultList();
    }    
}
