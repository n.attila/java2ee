package hu.oe.hoe.web;

import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.EmpireRepository;
import hu.oe.hoe.adatok.HeroRepository;
import hu.oe.hoe.adatok.Species;
import hu.oe.hoe.adatok.SpeciesRepository;
import hu.oe.hoe.adatok.User;
import hu.oe.hoe.adatok.UserRepository;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class ApplicationInit {
    
    @Produces
    @ApplicationScoped
    public UserRepository createUserRepository() {
        
        UserRepository ur = new UserRepository();
        
        if (ur.getUsers().size() == 0) {
            try {
                ur.Add(new User("a", "a", false));
            }
            catch(Exception e) {e.printStackTrace();}
        }
        
        return ur;
    }
    
    @Produces
    @ApplicationScoped
    public SpeciesRepository createSpeciesReopsitory() {
        SpeciesRepository sr = new SpeciesRepository();
        
        if (sr.getSpecies().size() == 0) {
            try {
                sr.Add(new Species("Ember", "okos"));
                sr.Add(new Species("Törpe", "kicsi"));
            }
            catch(Exception e) {e.printStackTrace();} 
        }
        
        return sr;
    }    

    @Produces
    @ApplicationScoped
    public HeroRepository createHeroReopsitory() {
        HeroRepository hr = new HeroRepository();
        
        return hr;
    }
    
    @Produces
    @ApplicationScoped
    public BuildingRepository createBuildingRepository(){
        BuildingRepository ur = new BuildingRepository();
    return ur;
    }
    @Produces
    @ApplicationScoped
    public EmpireRepository createEmpireRepository(){
        EmpireRepository ur = new EmpireRepository();
    return ur;
    }
    
    
}